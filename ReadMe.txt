Creators:
Filip Vlašić - filip.vlasic@fer.hr
Luka Gunjević - luka.gunjevic@outlook.com
Tea Deucht - tea.deucht@fer.hr
Marin Pavić - marin.pavic@fer.hr
Karla Car - kcar@arhitekt.hr

Installation:
1. download as zip
2. extract all
3. go to Build folder
4. run "Crazy Bunny Apocalypse.exe"
5. enjoy :)

YouTube:
https://youtu.be/U1Dqg0Dt3eE

OneDrive:
https://ferhr-my.sharepoint.com/:f:/g/personal/fv51091_fer_hr/EidsceSnJ-tApCfpq2HUV3gB0Yxp0VtE-iE3BTmNkWTAbA?e=Gukb4m

GitLab:
https://gitlab.com/Sicsile/crazy-bunny-apocalypse
